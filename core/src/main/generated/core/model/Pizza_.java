package core.model;

import core.model.Pizza.Cuisine;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Pizza.class)
public abstract class Pizza_ {

	public static volatile SingularAttribute<Pizza, Float> price;
	public static volatile SingularAttribute<Pizza, String> name;
	public static volatile SingularAttribute<Pizza, String> description;
	public static volatile SingularAttribute<Pizza, Cuisine> cuisine;
	public static volatile SingularAttribute<Pizza, Long> id;

}

