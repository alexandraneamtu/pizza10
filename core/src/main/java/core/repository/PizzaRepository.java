package core.repository;

import core.model.Pizza;

/**
 * Created by alexandraneamtu on 16/06/2017.
 */
public interface PizzaRepository extends Repository<Pizza,Long>{
}
