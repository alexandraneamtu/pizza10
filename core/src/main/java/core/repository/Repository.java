package core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * Created by alexandraneamtu on 16/06/2017.
 */

@NoRepositoryBean
@Transactional
public interface Repository<T,ID extends Serializable> extends JpaRepository<T,ID> {
}
