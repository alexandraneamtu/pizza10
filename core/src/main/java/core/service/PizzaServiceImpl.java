package core.service;

import core.model.Pizza;
import core.repository.PizzaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by alexandraneamtu on 16/06/2017.
 */
@Service
public class PizzaServiceImpl implements PizzaService {

    private static final Logger log = LoggerFactory.getLogger(PizzaServiceImpl.class);

    @Autowired
    private PizzaRepository pizzaRepository;

    @Override
    public List<Pizza> findAll() {
        log.trace("findAll");
        List<Pizza> pizzas = pizzaRepository.findAll();
        log.trace("findAll: pizzas={}", pizzas);
        return pizzas;
    }

    @Override
    public Pizza findPizza(Long id) {
        Pizza pizza = pizzaRepository.findOne(id);
        return pizza;
    }

    @Override
    public Pizza addPizza(String name, String description, float price, Pizza.Cuisine cuisine) {
        log.trace("createPizza: name={}, description={} price={} cuisine={}", name, description,price,cuisine);

        Pizza pizza = new Pizza(name,description,price,cuisine);
        Pizza db_pizza = pizzaRepository.save(pizza);
        log.trace("createPizza: pizza={}",db_pizza);

        return db_pizza;
    }
}
