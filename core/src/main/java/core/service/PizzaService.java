package core.service;

import core.model.Pizza;

import java.util.List;

/**
 * Created by alexandraneamtu on 16/06/2017.
 */
public interface PizzaService {
    List<Pizza> findAll();

    Pizza findPizza(Long id);

    Pizza addPizza(String name, String description, float price,Pizza.Cuisine cuisine);
}
