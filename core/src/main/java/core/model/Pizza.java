package core.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by alexandraneamtu on 16/06/2017.
 */

@Entity
@Table(name="pizza")
@Getter
@Setter
public class Pizza {

    public enum Cuisine{
        MEDITERRANEAN,
        ORIENTAL
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pizza_id")
    private Long id;

    @Column(name="name",unique = true)
    private String name;

    @Column(name="description")
    private String description;

    @Column(name="price")
    private float price;

    @Column(name="cuisine")
    @Enumerated(EnumType.STRING)
    private Cuisine cuisine;


    public Pizza(){}

    public Pizza(String name, String description,float price,Cuisine cuisine){
        this.name = name;
        this.description = description;
        this.price = price;
        this.cuisine = cuisine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pizza pizza = (Pizza) o;

        if (Float.compare(pizza.price, price) != 0) return false;
        if (id != null ? !id.equals(pizza.id) : pizza.id != null) return false;
        if (name != null ? !name.equals(pizza.name) : pizza.name != null) return false;
        if (description != null ? !description.equals(pizza.description) : pizza.description != null) return false;
        return cuisine != null ? cuisine.equals(pizza.cuisine) : pizza.cuisine == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (price != +0.0f ? Float.floatToIntBits(price) : 0);
        result = 31 * result + (cuisine != null ? cuisine.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", cuisine='" + cuisine + '\'' +
                '}';
    }
}
