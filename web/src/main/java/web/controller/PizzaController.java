package web.controller;

import core.model.Pizza;
import core.service.PizzaService;
import core.service.PizzaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.dto.MessageDTO;
import web.dto.PizzaDTO;
import web.dto.PizzasDTO;

import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by alexandraneamtu on 16/06/2017.
 */

@RestController
public class PizzaController {

    private static final Logger log = LoggerFactory.getLogger(PizzaController.class);

    @Autowired
    private PizzaService pizzaService;

    @RequestMapping(value = "pizzas",method = RequestMethod.GET)
    public PizzasDTO getPizzas(){
        log.trace("getAll");
        List<Pizza> pizzas = pizzaService.findAll();
        log.trace("Getall, pizzas={}",pizzas);
        return new PizzasDTO(pizzas.stream().map(s -> new PizzaDTO(s)).collect(Collectors.toList()));
    }

    @RequestMapping(value = "pizzas", method = RequestMethod.POST)
    public MessageDTO savePizza(@RequestBody Map<String,Pizza> content){
        Pizza p =  content.get("pizza");
        Pizza pizza;
        log.trace("createPizza: name={}, description={} price={}", p.getName(), p.getDescription(), p.getPrice());
        if(p.getName().length()<3 || p.getDescription().length()==0 || p.getPrice()<0)
            return new MessageDTO("Error!","Pizza not inserted in the database!Invalid Arguments!");
        try {
            pizza = pizzaService.addPizza(p.getName(), p.getDescription(), p.getPrice(),p.getCuisine());
        }
        catch (Exception e){
            return new MessageDTO("Error!","Pizza was not inserted in the database!");
        }
        log.trace("createPizza: pizza={}",pizza);
        return new MessageDTO("Success!","Pizza inserted into database!");
    }


    @RequestMapping(value = "pizzas/{type}", method = RequestMethod.POST)
    public PizzasDTO filerPizza(@PathVariable final String type, @RequestBody String number)
    {
        List<Pizza> pizzas;

        //log.trace(number);
        if(type.equals("price")){
            pizzas = pizzaService.findAll().stream()
                    .filter(a->a.getPrice()<= Float.valueOf(number))
                    .collect(Collectors.toList());

            pizzas.forEach(a-> System.out.println(a));
        }
        else if (type.equals("cuisine")){
            System.out.println("in cuisineeeeee   --------   "+number);
            pizzas = pizzaService.findAll().stream()
                    .filter(a->a.getCuisine().toString().toLowerCase().equals(number.toLowerCase()))
                    .collect(Collectors.toList());

            pizzas.forEach(a-> System.out.println(a));
        }
        else{
            String price = number.split(";")[0];
            String cuisine = number.split(";")[1];

            System.out.println(price);
            System.out.println(cuisine);
            pizzas = pizzaService.findAll().stream()
                    .filter(a->a.getPrice()<= Float.valueOf(price))
                    .filter(a->a.getCuisine().toString().toLowerCase().equals(cuisine.toLowerCase()))
                    .collect(Collectors.toList());

            pizzas.forEach(a-> System.out.println(a));
        }

        return new PizzasDTO(pizzas.stream().map(s -> new PizzaDTO(s)).collect(Collectors.toList()));
    }
}
