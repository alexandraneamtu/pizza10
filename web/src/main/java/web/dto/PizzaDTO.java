package web.dto;

import core.model.Pizza;

/**
 * Created by alexandraneamtu on 16/06/2017.
 */

public class PizzaDTO {
    private Long id;
    private String name;
    private String description;
    private float price;
    private Pizza.Cuisine cuisine;

    public PizzaDTO(){}

    public PizzaDTO(Long id, String name, String description, float price, Pizza.Cuisine cuisine){
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.cuisine = cuisine;
    }

    public PizzaDTO(Pizza p){
        this.id = p.getId();
        this.name = p.getName();
        this.description = p.getDescription();
        this.price = p.getPrice();
        this.cuisine = p.getCuisine();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Pizza.Cuisine getCuisine() {
        return cuisine;
    }

    public void setCuisine(Pizza.Cuisine cuisine) {
        this.cuisine = cuisine;
    }
}
