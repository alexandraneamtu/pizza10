package web.dto;

import java.util.List;
import java.util.Set;

/**
 * Created by alexandraneamtu on 16/06/2017.
 */
public class PizzasDTO {
    private List<PizzaDTO> pizzas;

    public PizzasDTO() {}

    public PizzasDTO(List<PizzaDTO> pizzas) {
        this.pizzas = pizzas;
    }

    public List<PizzaDTO> getPizzas() {
        return pizzas;
    }

    public void setPizzas(List<PizzaDTO> pizzas) {
        this.pizzas = pizzas;
    }
}
