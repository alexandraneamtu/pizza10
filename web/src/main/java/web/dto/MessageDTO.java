package web.dto;

/**
 * Created by alexandraneamtu on 16/06/2017.
 */
public class MessageDTO {
    private String message;
    private String type;

    public MessageDTO() {
    }

    public MessageDTO(String type, String message) {
        this.message = message;
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}