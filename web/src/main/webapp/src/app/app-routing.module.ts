import {Routes, RouterModule} from "@angular/router";
import {PizzaComponent} from "./pizza/pizza.component";
import {NgModel} from "@angular/forms";
import {NgModule} from "@angular/core";
import {FilterPizzaComponent} from "./filter-pizza/filter-pizza.component";
const routes:Routes = [
  {
    path:"new",
    component: PizzaComponent
  },
  {
    path:"filter",
    component: FilterPizzaComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
