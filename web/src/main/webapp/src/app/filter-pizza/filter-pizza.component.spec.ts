import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterPizzaComponent } from './filter-pizza.component';

describe('FilterPizzaComponent', () => {
  let component: FilterPizzaComponent;
  let fixture: ComponentFixture<FilterPizzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterPizzaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterPizzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
