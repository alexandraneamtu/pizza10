import {Injectable, OnInit} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import {Observable} from "rxjs";
import {Pizza} from "./pizza-model";

@Injectable()
export class PizzaService {
  private pizzaUrl = 'http://localhost:8080/api/pizzas';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http:Http){}

  extractPizzaData(res:Response){
    let body = res.json();
    return body || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

  create(name: string, description: string, price: number,cuisine: string): Observable<Pizza>{
    let pizza = {name, description, price, cuisine};
    console.log(JSON.stringify({"pizza": pizza}));

    return this.http
      .post(this.pizzaUrl,JSON.stringify({"pizza": pizza}),{headers:this.headers})
      .map(this.extractPizzaData)
      .catch(this.handleError)
  }

  private extractData(res: Response) {
    let body = res.json();
    console.log(body);
    return body.pizzas || {};
  }

  getPizzas():Observable<Pizza[]>{
    return this.http.get(this.pizzaUrl)
      .map(this.extractData)
      .catch(this.handleError);
  }

  filter(type:string,no:string):Observable<Pizza[]>{
    const url = `${this.pizzaUrl}/${type}`;
    console.log(url);

    return this.http.post(url,no,{headers:this.headers})
      .map(this.extractData)
      .catch(this.handleError);
  }





}
