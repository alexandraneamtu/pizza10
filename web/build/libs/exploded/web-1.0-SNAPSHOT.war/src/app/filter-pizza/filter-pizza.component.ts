import { Component, OnInit } from '@angular/core';
import {PizzaService} from "../shared/pizza.service";
import {Pizza} from "../shared/pizza-model";

@Component({
  selector: 'app-filter-pizza',
  templateUrl: './filter-pizza.component.html',
  styleUrls: ['./filter-pizza.component.css']
})
export class FilterPizzaComponent implements OnInit {

  pizzas: Pizza[];
  priceLessThanCheckbox : boolean=false;
  cuisineCheckbox: boolean=false;
  priceLessThanValue: string='';
  cuisineValue: string='';


  constructor(private pizzaService:PizzaService) { }

  ngOnInit() {
  }

  filter():void{
    console.log("CheckBoxes:",this.priceLessThanCheckbox,this.cuisineCheckbox);
    if(this.priceLessThanCheckbox == true && this.cuisineCheckbox == false){
      if(this.priceLessThanValue==='')
        alert("You must insert a value")
      console.log("Value:",this.priceLessThanValue)
      this.pizzaService.filter("price",this.priceLessThanValue).subscribe(pizzas => this.pizzas = pizzas);
    }
    else if(this.priceLessThanCheckbox == false && this.cuisineCheckbox == true){
      if(this.cuisineValue==='')
        alert("You must insert a value")
      console.log("Value:",this.cuisineValue)
      this.pizzaService.filter("cuisine",this.cuisineValue).subscribe(pizzas => this.pizzas = pizzas);
    }
    else if(this.priceLessThanCheckbox == true && this.cuisineCheckbox == true){
      if(this.cuisineValue==='' || this.priceLessThanValue==='')
        alert("You must insert a value")
      console.log("Value:",this.priceLessThanValue,this.cuisineValue)
      let finalvalue = this.priceLessThanValue +";"+ this.cuisineValue;
      console.log(finalvalue);
      this.pizzaService.filter("price&filter",finalvalue).subscribe(pizzas => this.pizzas = pizzas);
    }
    else{
      console.log("both unchecked")
      this.pizzaService.getPizzas().subscribe(pizzas => this.pizzas = pizzas)
    }

  }

}
