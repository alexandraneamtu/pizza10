import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PizzaComponent } from './pizza/pizza.component';
import {PizzaService} from "./shared/pizza.service";
import {RouterModule} from "@angular/router";
import {AppRoutingModule} from "./app-routing.module";
import { FilterPizzaComponent } from './filter-pizza/filter-pizza.component';

@NgModule({
  declarations: [
    AppComponent,
    PizzaComponent,
    FilterPizzaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [PizzaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
