import { Component, OnInit } from '@angular/core';
import {PizzaService} from "../shared/pizza.service";

@Component({
  selector: 'app-pizza',
  templateUrl: './pizza.component.html',
  styleUrls: ['./pizza.component.css']
})
export class PizzaComponent implements OnInit {
  private toAdd:boolean = false;
  private message:String = '';

  constructor(private pizzaService:PizzaService) { }

  ngOnInit() {
  }

  save(name, description, price, cuisine):void {
    console.log("save",name,description,price,cuisine);
    this.pizzaService.create(name,description,price,cuisine).subscribe(data => this.showMessage(data));
  }

  showMessage(data){
    this.toAdd = false;
    console.log("message to show:",data);
    this.message = data.type +" "+ data.message ;
  }

  add():void{
    this.toAdd = true;
  }

}
